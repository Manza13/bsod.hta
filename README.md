# bsod.hta
--> Español:

Testeado en Windows 10.
La pantalla bsod.hta es la mas frecuente en Windows, y por ellos os dejo un el codigo fuente. Es un script, que lo que genera una simulacion lo mas parecido o practicamente es igual.

    Para poder cerrarlo, sera necesario pulsar las teclas:
    alt + f4
    
--> English:

Tested in Windows 10.
The screen bsod.hta is the most frequent in Windows, and for them I leave a source code. It is a script, that what generates a simulation as similar or practically is the same.

    To be able to close it, it will be necessary to press the keys:
    alt + f4
    
